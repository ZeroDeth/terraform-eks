
resource "random_id" "this" {
  byte_length = "10"
}

################# CREATING THE REMOTE S3 BUCKET
resource "aws_s3_bucket" "remote-state" {
  #count         = var.create ? 0 : 1
  bucket        = "${var.region}-terraform-state-${random_id.this.hex}"
  acl           = "private"
  region        = var.region
  force_destroy = var.force_destroy

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  versioning {
    enabled = var.versioning
  }
}

################# CREATING THE DYNAMODB STATE LOCK  #######
resource "aws_dynamodb_table" "terraform_locks" {
  #count         = var.create ? 0 : 1
  name         = "${var.region}-dynamodb-locking-${random_id.this.hex}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}