provider "aws" {
  region = var.region
}

################# DATA SOURCES
data "aws_eks_cluster" "sample-cluster" {
  name = module.eks-sample-cluster.cluster_id
}

data "aws_eks_cluster_auth" "sample-cluster" {
  name = module.eks-sample-cluster.cluster_id
}

data "aws_availability_zones" "available" {
}


#this has to be there, it is the auth token that allows communication with eks-cluster
provider "kubernetes" {
  host                   = data.aws_eks_cluster.sample-cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.sample-cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.sample-cluster.token
  load_config_file       = false
  version                = "~> 1.9"
}

################# EKS CLUSTER
module "eks-sample-cluster" {
  #source       = "./modules/terraform-aws-eks-master"
  source                              = "terraform-aws-modules/eks/aws"
  cluster_name                        = var.cluster-name
  subnets                             = module.vpc.private_subnets
  vpc_id                              = module.vpc.vpc_id
  cluster_endpoint_private_access     = false
  cluster_endpoint_public_access      = true

  worker_groups = [
    {
      instance_type        = "t2.small"
      asg_max_size         = 3
      asg_min_size         = 2
      asg_desired_capacity = 2
      autoscaling_enabled  = true

    }
  ]
}

locals {
  cluster-name = var.cluster-name
  region       = var.region
}

# run 'aws eks update-kubeconfig ...' locally and update local kube config
resource "null_resource" "update_kubeconfig" {
  depends_on = [module.eks-sample-cluster]

  provisioner "local-exec" {
    command = "aws eks update-kubeconfig --name ${local.cluster-name} --region ${local.region}"
  }
}


#################### HELM
resource "null_resource" "install-helm" {
  depends_on = [module.eks-sample-cluster, null_resource.update_kubeconfig]

  provisioner "local-exec" {
    command = "./scripts/install-helm.sh"
  }
}


#################### ALB INGRESS
# WARNING: can't destroy internet gateway when installed aws-alb-ingress-controller
# https://github.com/terraform-providers/terraform-provider-aws/issues/9101

# resource "aws_iam_role_policy_attachment" "alb_ingress_policy_attachment" {
#   role       = module.eks-sample-cluster.worker_iam_role_name
#   policy_arn = aws_iam_policy.alb_ingress_policy.arn
# }

# data "template_file" "alb-ingress-policy" {
#   template = "${file("./scripts/alb-ingress-policy.tpl")}"
# }

# resource "aws_iam_policy" "alb_ingress_policy" {
#   name   = "alb-ingress-policy"
#   policy = data.template_file.alb-ingress-policy.rendered
# }

# resource "null_resource" "install_aws_alb_ingress_controller" {
#   depends_on = [null_resource.install-helm]

#   # sleep 60 seconds and wait for helm tiller deployed
#   provisioner "local-exec" {
#     command = "sleep 60; ./darwin-amd64/helm repo add incubator http://storage.googleapis.com/kubernetes-charts-incubator; ./darwin-amd64/helm install incubator/aws-alb-ingress-controller --set autoDiscoverAwsRegion=true --set autoDiscoverAwsVpcID=true --set clusterName=${local.cluster-name} --generate-name"

#   }
# }



####################### VPC MODULE
module "vpc" {
  #source = "./modules/terraform-aws-vpc-master"
  source = "terraform-aws-modules/vpc/aws"

  name                                    = "test-vpc"
  cidr                                    = var.cidr-block
  azs                                     = data.aws_availability_zones.available.names
  private_subnets                         = var.private-subnets
  public_subnets                          = var.public-subnets
  enable_nat_gateway                      = true
  single_nat_gateway                      = true
  enable_dns_hostnames                    = true
  create_database_subnet_route_table      = true
  create_database_internet_gateway_route  = true
  enable_dns_support                      = true
  
 
  tags = {
    Name                                            = "${var.cluster-name}-vpc"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
  }
  public_subnet_tags = {
    Name                                            = "${var.cluster-name}-eks-public"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
    "kubernetes.io/role/elb"                        = 1
  }
  private_subnet_tags = {
    Name                                            = "${var.cluster-name}-eks-private"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
    "kubernetes.io/role/internal-elb"               = 1
  }

}

####################### REMOTE STATE LOCKING
# module "remote-state-locking" {
#   source = "./modules/remote-state-locking"
#   region = var.region
# }

########################### USERS AND PERMISSIONS

module "iam_group_with_policies" {
  source = "./modules/terraform-aws-iam-master/modules/iam-group-with-policies"
  name   = var.group-name
  group_users = [
    module.iam_user_programmatic.this_iam_user_name,
    module.iam_user_console.this_iam_user_name
  ]
  custom_group_policy_arns = [
    "arn:aws:iam::aws:policy/AdministratorAccess",
  ]
}
######## Programmatic access (access key and secret key)
module "iam_user_programmatic" {
  source                        = "./modules/terraform-aws-iam-master/modules/iam-user"
  name                          = var.programmatic-user
  force_destroy                 = true
  pgp_key                       = var.pgp-key
  create_iam_user_login_profile = false
  create_iam_access_key         = true
}
######## AWS Management Console access (username and password)
module "iam_user_console" {
  source                        = "./modules/terraform-aws-iam-master/modules/iam-user"
  name                          = var.console-user
  force_destroy                 = true
  pgp_key                       = var.pgp-key
  password_reset_required       = false
  password_length               = var.password_length
  create_iam_user_login_profile = true
  create_iam_access_key         = false
}

################# AUTOMATING REMOTE STATE LOCKING
# data "template_file" "remote-state" {
#   template = "${file("./scripts/remote-state.tpl")}"
#   vars = {
#     s3-bucket      = module.remote-state-locking.bucket_name
#     dynamodb_table = module.remote-state-locking.dynamodb_table
#   }
# }
# resource "null_resource" "remote-state-locks" {
#   depends_on = [module.remote-state-locking, data.template_file.remote-state]
#   provisioner "local-exec" {
#     command = "sleep 20;cat > backend.tf <<EOL\n${data.template_file.remote-state.rendered}"
#   }
# }


########################### ARGO CD INSTALLATION
resource "null_resource" "install_argocd" {
  depends_on = [module.eks-sample-cluster, null_resource.update_kubeconfig]
  
  provisioner "local-exec" {
    command = "chmod +x scripts/argocd/argocd-installation.sh; scripts/argocd/argocd-installation.sh;sleep 10;kubectl apply -f scripts/argocd/argocd-configmap.yaml"
  }  
}

#before this can run, it needs a loadbalancer and i need to login to the cli
#argocd login ad971d628405a44f3855b4741a52d361-11934858.us-west-2.elb.amazonaws.com then prompt for username and password
#add private repo
# resource "null_resource" "private_repo" {
#   depends_on = [module.eks-sample-cluster, null_resource.update_kubeconfig, null_resource.install_argocd]
  
#   provisioner "local-exec" {
#     command = "argocd repo add https://git.deimos.co.za/praise/gitops.git --username gitops-token --password fdthkKv3RvyWzs9-wSaY"
#   }  
# }

#pull in the applications from argocd repo for gitops
resource "null_resource" "install_gitops" {
  depends_on = [module.eks-sample-cluster, null_resource.update_kubeconfig, null_resource.install-helm, null_resource.install_argocd] #null_resource.install-helm,
  
  provisioner "local-exec" {
    #command = "kubectl apply -f https://git.deimos.co.za/api/v4/projects/praise%2Fgitops/repository/files/argocd%2Fargocd.yaml/raw?private_token=W9KNe6dYTHMcZLpbsXQ9&ref=master"
    command = "kubectl apply -f https://tinyurl.com/y84xdsjo" #shortened because it was tripping off the last part
  }  
}

#check certmanager
resource "null_resource" "verify_certmanager" {
  depends_on = [null_resource.install_gitops]
  
  provisioner "local-exec" {
    command = "chmod +x scripts/argocd/check-certmanager.sh; scripts/argocd/check-certmanager.sh"
  }  
}

########################### CERT-MANAGER INSTALLATION
#create the issuer and cert  CAN ADD THIS TO NGINX INGRESS
resource "null_resource" "install_certificate_issuer" {
  depends_on = [null_resource.install_argocd, null_resource.install_gitops, null_resource.verify_certmanager]
  
  provisioner "local-exec" {
    command = "sleep 20; kubectl apply -f scripts/certmanager-issuer.yaml"
  }  
}

########################### EXTERNAL DNS
data "template_file" "external-dns-policy" {
  template = "${file("./scripts/external-dns-policy.tpl")}"
}

resource "aws_iam_policy" "external-dns-policy" {
  name   = "external-dns-policy"
  policy = data.template_file.external-dns-policy.rendered
}
#attach role to worker nodes for external dns to access route 53
resource "aws_iam_role_policy_attachment" "external-dns-policy-attachment" {
  role       = module.eks-sample-cluster.worker_iam_role_name 
  policy_arn = aws_iam_policy.external-dns-policy.arn
}

###########################EXPOSE ARGOCD SERVER
#expose argocd server THIS SHOULD BE MANUAL OR I PORT FORWARD
resource "null_resource" "expose_argocd_server" {
  depends_on = [null_resource.install_argocd, null_resource.install_gitops, null_resource.install_certificate_issuer, aws_iam_role_policy_attachment.external-dns-policy-attachment]

  provisioner "local-exec" {
    command = "kubectl apply -f scripts/nginx-ingress.yaml" 
  }  
}



