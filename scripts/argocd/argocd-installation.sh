#!/bin/sh

#install ArgoCD
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
sleep 20

#create a service of Type Loadbalancer to expose the nginx controller deployment outside the cluster
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "NodePort"}}'
#Access ArgoCD Api server
#kubectl port-forward svc/argocd-server -n argocd 8080:443

#TO get ARgoCD password and username="admin"
#kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2

