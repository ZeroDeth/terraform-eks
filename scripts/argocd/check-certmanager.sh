#!/bin/bash
sleep 30;
COMMAND=$(kubectl get pods -n cert-manager -l  app.kubernetes.io/name=cert-manager -o json | jq -j '.items[].status.phase')
while true; do
    if [[ $COMMAND != "Running" ]];
        then      
           kubectl get pods -n cert-manager -l  app.kubernetes.io/name=cert-manager -o json | jq -j '.items[].status.phase' &>/dev/null
    else
      echo 'All Done'
      break
    fi
done