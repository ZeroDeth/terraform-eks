 terraform {
  backend "s3" {
    bucket = "${s3-bucket}"
    dynamodb_table = "${dynamodb_table}"
    region = "us-west-2"
    key = "global/terraform.tfstate"
  }
 }

